import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Data/data.dart';
import 'ViewAll.dart';

class MusicStyle extends StatelessWidget {
  final List<Challenges> chalengs;

  const MusicStyle({super.key, required this.chalengs});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        children: [
          viewAll(
            onTap: () {
              print('مشاهده همه');
            },
          ),
          const Padding(
              padding: EdgeInsets.only(right: 14),
              child: Text(
                "ویژه های نت نت",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              )),
        ],
      ),
      SizedBox(
        height: 24,
      ),
      SizedBox(
        height: MediaQuery.of(context).size.height / 9,
        child: ListView.builder(
            reverse: true,
            itemCount: chalengs.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              return Padding(
                  padding: index == 0
                      ? EdgeInsets.only(right: 12, left: 12)
                      : EdgeInsets.only(left: 12),
                  child: Stack(children: [
                    Container(
                      width: 100,
                      height: 200,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(220),
                        image: DecorationImage(
                            image: NetworkImage("https://notenet.ir" +
                                chalengs[index].artist.bannerMobileImage),
                            fit: BoxFit.cover),
                      ),
                      child: Container(
                        margin: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(220),
                            color: Colors.red.withOpacity(0.9)),
                      ),
                    ),
                    const Positioned.fill(
                        child: Center(
                            child: Text(
                      'پاپ',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ))),
                  ]));
            }),
      ),
      SizedBox(
        height: 14,
      ),
    ]);
  }
}
