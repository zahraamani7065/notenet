import 'package:flutter/cupertino.dart';
import 'package:scroll_loop_auto_scroll/scroll_loop_auto_scroll.dart';

import '../Data/data.dart';
import 'ViewAll.dart';
import 'Vip.dart';

class BestVideos extends StatelessWidget {
  final List<Mostpopularvideo> bestVideos;

  const BestVideos({super.key, required this.bestVideos});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        children: [
          Row(
            children: [
              viewAll(
                onTap: () {
                  print('مشاهده همه');
                  print(bestVideos.length);
                },
              ),
              const Padding(
                  padding: EdgeInsets.only(right: 14),
                  child: Text(
                    "برترین های نت نت",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  )),
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Container(
            margin: const EdgeInsets.only(right: 12, left: 12),
            height: MediaQuery.of(context).size.height / 2.3,
            child: GridView.builder(
                reverse: true,
                scrollDirection: Axis.horizontal,
                itemCount: bestVideos.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.2,
                    crossAxisSpacing: 6,
                    mainAxisSpacing: 12),
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: Image.network(
                            "https://notenet.ir" + bestVideos[index].posterPath,
                            fit: BoxFit.cover,
                          )),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 2, right: 2),
                        child: Row(
                          children: [
                            icon(
                              onTap: () {
                                print('play');
                              },
                            ),
                            Flexible(
                                child: Text(
                              bestVideos[index].videoCaption,
                              overflow: TextOverflow.ellipsis,
                            )),
                          ],
                        ),
                      )
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }
}
