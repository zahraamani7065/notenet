import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class viewAll extends StatelessWidget {
  final Function() onTap;

  const viewAll({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: InkWell(
            onTap: onTap,
            child: Text('مشاهده همه'),
          )),
    );
  }
}
