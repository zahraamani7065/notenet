import 'package:flutter/cupertino.dart';
import 'package:notenet/Widgets/Banner.dart';

import '../Data/data.dart';
import '../carousel/carousel_slider.dart';

class CarouselSliders extends StatelessWidget {
  final List<Bannerr> banner;

  const CarouselSliders({
    Key? key,
    required this.banner,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider.builder(
        itemCount: banner.length,
        itemBuilder: (context, index, realIndex) {
          return Banners(
            bannerItems: banner,
            left: realIndex == 0 ? 12 : 8,
            right: realIndex == banner.length ? 12 : 1,
            index: index,
          );
        },
        options: CarouselOptions(
            reverse: true,
            viewportFraction: 0.8,
            aspectRatio: 1.2,
            initialPage: 0,
            disableCenter: true,
            enableInfiniteScroll: false,
            enlargeCenterPage: true,
            scrollPhysics: const BouncingScrollPhysics(),
            enlargeStrategy: CenterPageEnlargeStrategy.height));
  }
}
