import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notenet/Data/data.dart';

class Banners extends StatelessWidget {
  final int index;
  final double left;
  final double right;
  final List<Bannerr> bannerItems;

  const Banners(
      {super.key,
      required this.left,
      required this.right,
      required this.bannerItems,
      required this.index});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          left: left,
          right: right,
          child: Container(
            margin: const EdgeInsets.fromLTRB(0, 0, 0, 16),
            decoration: BoxDecoration(
                color: Colors.blue, borderRadius: BorderRadius.circular(32)),
            foregroundDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(32),
                gradient: const LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.center,
                  colors: [Color(0xff0D253C), Colors.transparent],
                )),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(32),
                child: Image.network(
                  "https://notenet.ir" + bannerItems[index].bannerImage,
                  fit: BoxFit.fill,
                )),
          ),
        ),
        Positioned(
            bottom: 48,
            right: 56,
            child: Text(
              bannerItems[index].bannerType + "سیروان خسروی",
              style: TextStyle(color: Colors.white38),
            ))
      ],
    );
  }
}
