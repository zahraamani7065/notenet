import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notenet/Data/data.dart';

import 'ViewAll.dart';

class VipMusic extends StatelessWidget {
  final List<VipVideos> VIPlst;

  const VipMusic({super.key, required this.VIPlst});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            children: [
              viewAll(
                onTap: () {
                  print('مشاهده همه');
                  print(VIPlst.length);
                },
              ),
              const Padding(
                  padding: EdgeInsets.only(right: 14),
                  child: Text(
                    "ویژه های نت نت",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  )),
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Container(
            margin: EdgeInsets.only(right: 12, left: 12),
            height: MediaQuery.of(context).size.height / 2.3,
            child: GridView.builder(
                reverse: true,
                scrollDirection: Axis.horizontal,
                itemCount: VIPlst.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.2,
                    crossAxisSpacing: 6,
                    mainAxisSpacing: 12),
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      ClipRRect(
                          borderRadius: BorderRadius.circular(4),
                          child: Image.network(
                            "https://notenet.ir" + VIPlst[index].posterPath,
                            fit: BoxFit.cover,
                          )),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 2, right: 2),
                        child: Row(
                          children: [
                            icon(
                              onTap: () {
                                print('play');
                              },
                            ),
                            Flexible(
                                child: Text(
                              VIPlst[index].videoCaption,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: 16,
                              ),
                            )),
                          ],
                        ),
                      )
                    ],
                  );
                }),
          ),
        ],
      ),
    );
  }
}

class icon extends StatelessWidget {
  final Function() onTap;

  const icon({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Align(
        alignment: Alignment.centerLeft,
        child: InkWell(
          onTap: onTap,
          child: const Icon(Icons.music_note),
        ),
      ),
    );
  }
}
