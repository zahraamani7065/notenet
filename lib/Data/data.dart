import 'dart:convert';
import 'package:http/http.dart' as http;
class AutoGenerate {
  AutoGenerate({
    required this.artists,
    required this.artistsLength,
    required this.banner,
    required this.categoriesCounts,
    required this.category,
    required this.challenge,
    required this.challengeTotalCount,
    required this.code,
    required this.domain,
    required this.metaDescription,
    required this.mhVideos,
    required this.mostpopularvideo,
    required this.mostpopularvideoLength,
    required this.mostviewedvideos,
    required this.mostviewedvideosLength,
    required this.newestvideo,
    required this.newestvideoLength,
    required this.referer,
    required this.specialCounts,
    required this.specials,
    required this.user,
    required this.vipVideos,
    required this.vipVideosLength,
  });
  late final List<Artists> artists;
  late final int artistsLength;
  late final List<Bannerr> banner;
  late final int categoriesCounts;
  late final List<Category> category;
  late final List<Challenges> challenge;
  late final int challengeTotalCount;
  late final String code;
  late final String domain;
  late final String metaDescription;
  late final int mhVideos;
  late final List<Mostpopularvideo> mostpopularvideo;
  late final int mostpopularvideoLength;
  late final List<Mostviewedvideos> mostviewedvideos;
  late final int mostviewedvideosLength;
  late final List<Newestvideo> newestvideo;
  late final int newestvideoLength;
  late final String referer;
  late final int specialCounts;
  late final List<dynamic> specials;
  late final User user;
  late final List<VipVideos> vipVideos;
  late final int vipVideosLength;

  AutoGenerate.fromJson(Map<String, dynamic> json){
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    artistsLength = json['artists_length'];
    banner = List.from(json['banner']).map((e)=>Bannerr.fromJson(e)).toList();
    categoriesCounts = json['categories_counts'];
    category = List.from(json['category']).map((e)=>Category.fromJson(e)).toList();
    challenge = List.from(json['challenge']).map((e)=>Challenges.fromJson(e)).toList();
    challengeTotalCount = json['challenge_total_count'];
    code = json['code'];
    domain = json['domain'];
    metaDescription = json['meta_description'];
    mhVideos = json['mh_videos'];
    mostpopularvideo = List.from(json['mostpopularvideo']).map((e)=>Mostpopularvideo.fromJson(e)).toList();
    mostpopularvideoLength = json['mostpopularvideo_length'];
    mostviewedvideos = List.from(json['mostviewedvideos']).map((e)=>Mostviewedvideos.fromJson(e)).toList();
    mostviewedvideosLength = json['mostviewedvideos_length'];
    newestvideo = List.from(json['newestvideo']).map((e)=>Newestvideo.fromJson(e)).toList();
    newestvideoLength = json['newestvideo_length'];
    referer = json['referer'];
    specialCounts = json['special_counts'];
    specials = List.castFrom<dynamic, dynamic>(json['specials']);
    user = User.fromJson(json['user']);
    vipVideos = List.from(json['vip_videos']).map((e)=>VipVideos.fromJson(e)).toList();
    vipVideosLength = json['vip_videos_length'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['artists_length'] = artistsLength;
    _data['banner'] = banner.map((e)=>e.toJson()).toList();
    _data['categories_counts'] = categoriesCounts;
    _data['category'] = category.map((e)=>e.toJson()).toList();
    _data['challenge'] = challenge.map((e)=>e.toJson()).toList();
    _data['challenge_total_count'] = challengeTotalCount;
    _data['code'] = code;
    _data['domain'] = domain;
    _data['meta_description'] = metaDescription;
    _data['mh_videos'] = mhVideos;
    _data['mostpopularvideo'] = mostpopularvideo.map((e)=>e.toJson()).toList();
    _data['mostpopularvideo_length'] = mostpopularvideoLength;
    _data['mostviewedvideos'] = mostviewedvideos.map((e)=>e.toJson()).toList();
    _data['mostviewedvideos_length'] = mostviewedvideosLength;
    _data['newestvideo'] = newestvideo.map((e)=>e.toJson()).toList();
    _data['newestvideo_length'] = newestvideoLength;
    _data['referer'] = referer;
    _data['special_counts'] = specialCounts;
    _data['specials'] = specials;
    _data['user'] = user.toJson();
    _data['vip_videos'] = vipVideos.map((e)=>e.toJson()).toList();
    _data['vip_videos_length'] = vipVideosLength;
    return _data;
  }
}

class Artists {
  Artists({
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.bio,
    this.buttons,
    required this.enabledPanel,
    required this.id,
    required this.link,
    required this.nameEn,
    required this.nameFa,
    required this.profileImage,
    required this.published,
    required this.totalVideoLiked,
    required this.verify,
  });
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String bio;
  late final List<Buttons>? buttons;
  late final bool enabledPanel;
  late final String id;
  late final String link;
  late final String nameEn;
  late final String nameFa;
  late final String profileImage;
  late final bool published;
  late final int totalVideoLiked;
  late final bool verify;

  Artists.fromJson(Map<String, dynamic> json){
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    bio = json['bio'];
    buttons = null;
    enabledPanel = json['enabled_panel'];
    id = json['id'];
    link = json['link'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    profileImage = json['profile_image'];
    published = json['published'];
    totalVideoLiked = json['total_video_liked'];
    verify = json['verify'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['bio'] = bio;
    _data['buttons'] = buttons;
    _data['enabled_panel'] = enabledPanel;
    _data['id'] = id;
    _data['link'] = link;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['profile_image'] = profileImage;
    _data['published'] = published;
    _data['total_video_liked'] = totalVideoLiked;
    _data['verify'] = verify;
    return _data;
  }
}

class Buttons {
  Buttons({
    required this.link,
    required this.name,
  });
  late final String link;
  late final String name;

  Buttons.fromJson(Map<String, dynamic> json){
    link = json['link'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['link'] = link;
    _data['name'] = name;
    return _data;
  }
}

class Bannerr {
  Bannerr({
    required this.bannerImage,
    required this.bannerMobileImage,
    required this.bannerType,
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.position,
    required this.published,
    required this.videoLink,
  });
  late final String bannerImage;
  late final String bannerMobileImage;
  late final String bannerType;
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final int position;
  late final bool published;
  late final String videoLink;

  Bannerr.fromJson(Map<String, dynamic> json){
    bannerImage = json['banner_image'];
    bannerMobileImage = json['banner_mobile_image'];
    bannerType = json['banner_type'];
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    position = json['position'];
    published = json['published'];
    videoLink = json['video_link'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_image'] = bannerImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['banner_type'] = bannerType;
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['position'] = position;
    _data['published'] = published;
    _data['video_link'] = videoLink;
    return _data;
  }
}

class Category {
  Category({
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.id,
    required this.logoPath,
    required this.mobileIcon,
    required this.nameEn,
    required this.nameFa,
    required this.posterPath,
    required this.published,
    required this.special,
  });
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String id;
  late final String logoPath;
  late final String mobileIcon;
  late final String nameEn;
  late final String nameFa;
  late final String posterPath;
  late final bool published;
  late final bool special;

  Category.fromJson(Map<String, dynamic> json){
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    id = json['id'];
    logoPath = json['logo_path'];
    mobileIcon = json['mobile_icon'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    posterPath = json['poster_path'];
    published = json['published'];
    special = json['special'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['id'] = id;
    _data['logo_path'] = logoPath;
    _data['mobile_icon'] = mobileIcon;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['special'] = special;
    return _data;
  }
}

class Challenges {
  Challenges({
    required this.activated,
    required this.artist,
    required this.createdAt,
    required this.description,
    required this.id,
    required this.mobileBannerPath,
    required this.name,
    required this.published,
    required this.questions,
    required this.startupVideo,
  });
  late final bool activated;
  late final Artist artist;
  late final String createdAt;
  late final String description;
  late final String id;
  late final String mobileBannerPath;
  late final String name;
  late final bool published;
  late final List<Questions> questions;
  late final StartupVideo startupVideo;

  Challenges.fromJson(Map<String, dynamic> json){
    activated = json['activated'];
    artist = Artist.fromJson(json['artist']);
    createdAt = json['created_at'];
    description = json['description'];
    id = json['id'];
    mobileBannerPath = json['mobile_banner_path'];
    name = json['name'];
    published = json['published'];
    questions = List.from(json['questions']).map((e)=>Questions.fromJson(e)).toList();
    startupVideo = StartupVideo.fromJson(json['startup_video']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['activated'] = activated;
    _data['artist'] = artist.toJson();
    _data['created_at'] = createdAt;
    _data['description'] = description;
    _data['id'] = id;
    _data['mobile_banner_path'] = mobileBannerPath;
    _data['name'] = name;
    _data['published'] = published;
    _data['questions'] = questions.map((e)=>e.toJson()).toList();
    _data['startup_video'] = startupVideo.toJson();
    return _data;
  }
}

class Artist {
  Artist({
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.bio,
    this.buttons,
    required this.enabledPanel,
    required this.id,
    required this.link,
    required this.nameEn,
    required this.nameFa,
    required this.profileImage,
    required this.published,
    required this.totalVideoLiked,
    required this.verify,
  });
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String bio;
  late final Null buttons;
  late final bool enabledPanel;
  late final String id;
  late final String link;
  late final String nameEn;
  late final String nameFa;
  late final String profileImage;
  late final bool published;
  late final int totalVideoLiked;
  late final bool verify;

  Artist.fromJson(Map<String, dynamic> json){
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    bio = json['bio'];
    buttons = null;
    enabledPanel = json['enabled_panel'];
    id = json['id'];
    link = json['link'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    profileImage = json['profile_image'];
    published = json['published'];
    totalVideoLiked = json['total_video_liked'];
    verify = json['verify'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['bio'] = bio;
    _data['buttons'] = buttons;
    _data['enabled_panel'] = enabledPanel;
    _data['id'] = id;
    _data['link'] = link;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['profile_image'] = profileImage;
    _data['published'] = published;
    _data['total_video_liked'] = totalVideoLiked;
    _data['verify'] = verify;
    return _data;
  }
}

class Questions {
  Questions({
    required this.answers,
    required this.artist,
    required this.id,
    required this.text,
    required this.video,
  });
  late final List<String> answers;
  late final Artist artist;
  late final String id;
  late final String text;
  late final Video video;

  Questions.fromJson(Map<String, dynamic> json){
    answers = List.castFrom<dynamic, String>(json['answers']);
    artist = Artist.fromJson(json['artist']);
    id = json['id'];
    text = json['text'];
    video = Video.fromJson(json['video']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['answers'] = answers;
    _data['artist'] = artist.toJson();
    _data['id'] = id;
    _data['text'] = text;
    _data['video'] = video.toJson();
    return _data;
  }
}

class Video {
  Video({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<dynamic> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<dynamic> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final Null playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<dynamic> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  Video.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.castFrom<dynamic, dynamic>(json['bands']);
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.castFrom<dynamic, dynamic>(json['company']);
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.castFrom<dynamic, dynamic>(json['toncodes']);
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company;
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes;
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext' ] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}

class Categories {
  Categories({
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.id,
    required this.logoPath,
    required this.mobileIcon,
    required this.nameEn,
    required this.nameFa,
    required this.posterPath,
    required this.published,
    required this.special,
  });
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String id;
  late final String logoPath;
  late final String mobileIcon;
  late final String nameEn;
  late final String nameFa;
  late final String posterPath;
  late final bool published;
  late final bool special;

  Categories.fromJson(Map<String, dynamic> json){
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    id = json['id'];
    logoPath = json['logo_path'];
    mobileIcon = json['mobile_icon'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    posterPath = json['poster_path'];
    published = json['published'];
    special = json['special'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['id'] = id;
    _data['logo_path'] = logoPath;
    _data['mobile_icon'] = mobileIcon;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['special'] = special;
    return _data;
  }
}

class Creator {
  Creator({
    required this.avatar,
    required this.cellphone,
    required this.email,
    required this.fullname,
    required this.gender,
    required this.group,
    required this.id,
    required this.ipgServiceName,
    required this.isAdmin,
    required this.marital,
    required this.martial,
    this.roles,
    required this.sex,
    required this.showableName,
    required this.verifiedMobile,
  });
  late final String avatar;
  late final String cellphone;
  late final String email;
  late final String fullname;
  late final String gender;
  late final String group;
  late final String id;
  late final String ipgServiceName;
  late final bool isAdmin;
  late final String marital;
  late final String martial;
  late final Null roles;
  late final String sex;
  late final String showableName;
  late final bool verifiedMobile;

  Creator.fromJson(Map<String, dynamic> json){
    avatar = json['avatar'];
    cellphone = json['cellphone'];
    email = json['email'];
    fullname = json['fullname'];
    gender = json['gender'];
    group = json['group'];
    id = json['id'];
    ipgServiceName = json['ipg_service_name'];
    isAdmin = json['is_admin'];
    marital = json['marital'];
    martial = json['martial'];
    roles = null;
    sex = json['sex'];
    showableName = json['showable_name'];
    verifiedMobile = json['verified_mobile'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['avatar'] = avatar;
    _data['cellphone'] = cellphone;
    _data['email'] = email;
    _data['fullname'] = fullname;
    _data['gender'] = gender;
    _data['group'] = group;
    _data['id'] = id;
    _data['ipg_service_name'] = ipgServiceName;
    _data['is_admin'] = isAdmin;
    _data['marital'] = marital;
    _data['martial'] = martial;
    _data['roles'] = roles;
    _data['sex'] = sex;
    _data['showable_name'] = showableName;
    _data['verified_mobile'] = verifiedMobile;
    return _data;
  }
}

class StartupVideo {
  StartupVideo({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<dynamic> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<dynamic> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final Null playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<dynamic> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  StartupVideo.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.castFrom<dynamic, dynamic>(json['bands']);
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.castFrom<dynamic, dynamic>(json['company']);
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.castFrom<dynamic, dynamic>(json['toncodes']);
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company;
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes;
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext'] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}

class Mostpopularvideo {
  Mostpopularvideo({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<dynamic> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<Company> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final List<PlayList>? playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<Toncodes> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  Mostpopularvideo.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.castFrom<dynamic, dynamic>(json['bands']);
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.from(json['company']).map((e)=>Company.fromJson(e)).toList();
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.from(json['toncodes']).map((e)=>Toncodes.fromJson(e)).toList();
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company.map((e)=>e.toJson()).toList();
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes.map((e)=>e.toJson()).toList();
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext'] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}

class Company {
  Company({
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.bio,
    required this.id,
    required this.nameEn,
    required this.nameFa,
    required this.profileImage,
    required this.published,
  });
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String bio;
  late final String id;
  late final String nameEn;
  late final String nameFa;
  late final String profileImage;
  late final bool published;

  Company.fromJson(Map<String, dynamic> json){
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    bio = json['bio'];
    id = json['id'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    profileImage = json['profile_image'];
    published = json['published'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['bio'] = bio;
    _data['id'] = id;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['profile_image'] = profileImage;
    _data['published'] = published;
    return _data;
  }
}

class PlayList {
  PlayList({
    required this.ID,
    required this.playListName,
    required this.playListCount,
    required this.playListImage,
    required this.published,
    required this.videos,
  });
  late final String ID;
  late final String playListName;
  late final int playListCount;
  late final String playListImage;
  late final bool published;
  late final List<String> videos;

  PlayList.fromJson(Map<String, dynamic> json){
    ID = json['ID'];
    playListName = json['play_list_name'];
    playListCount = json['play_list_count'];
    playListImage = json['play_list_image'];
    published = json['published'];
    videos = List.castFrom<dynamic, String>(json['videos']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ID'] = ID;
    _data['play_list_name'] = playListName;
    _data['play_list_count'] = playListCount;
    _data['play_list_image'] = playListImage;
    _data['published'] = published;
    _data['videos'] = videos;
    return _data;
  }
}

class Toncodes {
  Toncodes({
    required this.artistName,
    required this.id,
    required this.link,
    required this.musicName,
    required this.showableName,
    required this.toncode,
    required this.updatedAt,
  });
  late final String artistName;
  late final String id;
  late final String link;
  late final String musicName;
  late final String showableName;
  late final String toncode;
  late final String updatedAt;

  Toncodes.fromJson(Map<String, dynamic> json){
    artistName = json['artist_name'];
    id = json['id'];
    link = json['link'];
    musicName = json['music_name'];
    showableName = json['showable_name'];
    toncode = json['toncode'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_name'] = artistName;
    _data['id'] = id;
    _data['link'] = link;
    _data['music_name'] = musicName;
    _data['showable_name'] = showableName;
    _data['toncode'] = toncode;
    _data['updated_at'] = updatedAt;
    return _data;
  }
}

class Mostviewedvideos {
  Mostviewedvideos({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<Bands> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<Company> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final Null playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<Toncodes> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  Mostviewedvideos.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.from(json['bands']).map((e)=>Bands.fromJson(e)).toList();
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.from(json['company']).map((e)=>Company.fromJson(e)).toList();
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.from(json['toncodes']).map((e)=>Toncodes.fromJson(e)).toList();
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands.map((e)=>e.toJson()).toList();
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company.map((e)=>e.toJson()).toList();
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes.map((e)=>e.toJson()).toList();
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext'] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}

class Bands {
  Bands({
    required this.artists,
    required this.bannerDesktopImage,
    required this.bannerMobileImage,
    required this.bio,
    required this.id,
    required this.link,
    required this.nameEn,
    required this.nameFa,
    required this.profileImage,
    required this.published,
    required this.verify,
  });
  late final List<dynamic> artists;
  late final String bannerDesktopImage;
  late final String bannerMobileImage;
  late final String bio;
  late final String id;
  late final String link;
  late final String nameEn;
  late final String nameFa;
  late final String profileImage;
  late final bool published;
  late final bool verify;

  Bands.fromJson(Map<String, dynamic> json){
    artists = List.castFrom<dynamic, dynamic>(json['artists']);
    bannerDesktopImage = json['banner_desktop_image'];
    bannerMobileImage = json['banner_mobile_image'];
    bio = json['bio'];
    id = json['id'];
    link = json['link'];
    nameEn = json['name_en'];
    nameFa = json['name_fa'];
    profileImage = json['profile_image'];
    published = json['published'];
    verify = json['verify'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artists'] = artists;
    _data['banner_desktop_image'] = bannerDesktopImage;
    _data['banner_mobile_image'] = bannerMobileImage;
    _data['bio'] = bio;
    _data['id'] = id;
    _data['link'] = link;
    _data['name_en'] = nameEn;
    _data['name_fa'] = nameFa;
    _data['profile_image'] = profileImage;
    _data['published'] = published;
    _data['verify'] = verify;
    return _data;
  }
}

class Newestvideo {
  Newestvideo({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<Bands> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<dynamic> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final Null playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<dynamic> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  Newestvideo.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.from(json['bands']).map((e)=>Bands.fromJson(e)).toList();
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.castFrom<dynamic, dynamic>(json['company']);
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.castFrom<dynamic, dynamic>(json['toncodes']);
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands.map((e)=>e.toJson()).toList();
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company;
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes;
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext'] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}

class User {
  User({
    required this.albumAccess,
    required this.avatar,
    required this.birthday,
    required this.cellphone,
    required this.createdAt,
    required this.email,
    required this.fullname,
    required this.gender,
    required this.hasSubscription,
    required this.id,
    required this.isDeleted,
    required this.marital,
    required this.phone,
    this.playlist,
    required this.remainingDays,
    required this.showableName,
    required this.startSubscription,
    required this.subscriptionExpire,
    required this.ticke,
    required this.verifiedMobile,
  });
  late final List<dynamic> albumAccess;
  late final String avatar;
  late final String birthday;
  late final String cellphone;
  late final String createdAt;
  late final String email;
  late final String fullname;
  late final String gender;
  late final bool hasSubscription;
  late final String id;
  late final bool isDeleted;
  late final String marital;
  late final String phone;
  late final Null playlist;
  late final String remainingDays;
  late final String showableName;
  late final String startSubscription;
  late final String subscriptionExpire;
  late final List<dynamic> ticke;
  late final bool verifiedMobile;

  User.fromJson(Map<String, dynamic> json){
    albumAccess = List.castFrom<dynamic, dynamic>(json['album_access']);
    avatar = json['avatar'];
    birthday = json['birthday'];
    cellphone = json['cellphone'];
    createdAt = json['created_at'];
    email = json['email'];
    fullname = json['fullname'];
    gender = json['gender'];
    hasSubscription = json['has_subscription'];
    id = json['id'];
    isDeleted = json['is_deleted'];
    marital = json['marital'];
    phone = json['phone'];
    playlist = null;
    remainingDays = json['remaining_days'];
    showableName = json['showable_name'];
    startSubscription = json['start_subscription'];
    subscriptionExpire = json['subscription_expire'];
    ticke = List.castFrom<dynamic, dynamic>(json['ticke']);
    verifiedMobile = json['verified_mobile'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['album_access'] = albumAccess;
    _data['avatar'] = avatar;
    _data['birthday'] = birthday;
    _data['cellphone'] = cellphone;
    _data['created_at'] = createdAt;
    _data['email'] = email;
    _data['fullname'] = fullname;
    _data['gender'] = gender;
    _data['has_subscription'] = hasSubscription;
    _data['id'] = id;
    _data['is_deleted'] = isDeleted;
    _data['marital'] = marital;
    _data['phone'] = phone;
    _data['playlist'] = playlist;
    _data['remaining_days'] = remainingDays;
    _data['showable_name'] = showableName;
    _data['start_subscription'] = startSubscription;
    _data['subscription_expire'] = subscriptionExpire;
    _data['ticke'] = ticke;
    _data['verified_mobile'] = verifiedMobile;
    return _data;
  }
}

class VipVideos {
  VipVideos({
    required this.artistPosition,
    required this.artists,
    required this.bands,
    required this.categories,
    required this.challengeTeaser,
    required this.comments,
    required this.commentsCount,
    required this.company,
    required this.creator,
    required this.dislike,
    required this.enabled,
    this.hyperLinks,
    required this.id,
    required this.idBase64,
    required this.isKaraoke,
    required this.isKaraokeDownload,
    required this.isKaraokeFinal,
    required this.isLiked,
    required this.isNewest,
    required this.isQuestion,
    required this.isTeaser,
    required this.isTop,
    required this.isTranscode,
    required this.isUploaded,
    required this.karaokeId,
    required this.like,
    required this.liveVideo,
    this.meta,
    required this.musicians,
    required this.newestAt,
    required this.payable,
    this.playList,
    required this.poemFa,
    required this.position,
    required this.posterPath,
    required this.published,
    required this.rate,
    required this.readyToSendComment,
    required this.releaseDate,
    required this.toncodes,
    required this.uploadPath,
    required this.uploadedAt,
    required this.url,
    required this.videoCaption,
    required this.videoDemoPath,
    required this.videoDescription,
    required this.videoExt,
    required this.videoPrice,
    required this.videoPriceRial,
    required this.videoPriceToman,
    required this.videoSize,
    required this.videoStreamPath,
    required this.view,
  });
  late final int artistPosition;
  late final List<Artists> artists;
  late final List<dynamic> bands;
  late final List<Categories> categories;
  late final bool challengeTeaser;
  late final List<dynamic> comments;
  late final int commentsCount;
  late final List<Company> company;
  late final Creator creator;
  late final int dislike;
  late final bool enabled;
  late final Null hyperLinks;
  late final String id;
  late final String idBase64;
  late final bool isKaraoke;
  late final bool isKaraokeDownload;
  late final bool isKaraokeFinal;
  late final bool isLiked;
  late final bool isNewest;
  late final bool isQuestion;
  late final bool isTeaser;
  late final bool isTop;
  late final bool isTranscode;
  late final bool isUploaded;
  late final String karaokeId;
  late final int like;
  late final bool liveVideo;
  late final Null meta;
  late final List<dynamic> musicians;
  late final String newestAt;
  late final bool payable;
  late final Null playList;
  late final String poemFa;
  late final int position;
  late final String posterPath;
  late final bool published;
  late final String rate;
  late final String readyToSendComment;
  late final String releaseDate;
  late final List<dynamic> toncodes;
  late final String uploadPath;
  late final String uploadedAt;
  late final String url;
  late final String videoCaption;
  late final String videoDemoPath;
  late final String videoDescription;
  late final String videoExt;
  late final int videoPrice;
  late final String videoPriceRial;
  late final String videoPriceToman;
  late final int videoSize;
  late final String videoStreamPath;
  late final int view;

  VipVideos.fromJson(Map<String, dynamic> json){
    artistPosition = json['artist_position'];
    artists = List.from(json['artists']).map((e)=>Artists.fromJson(e)).toList();
    bands = List.castFrom<dynamic, dynamic>(json['bands']);
    categories = List.from(json['categories']).map((e)=>Categories.fromJson(e)).toList();
    challengeTeaser = json['challenge_teaser'];
    comments = List.castFrom<dynamic, dynamic>(json['comments']);
    commentsCount = json['comments_count'];
    company = List.from(json['company']).map((e)=>Company.fromJson(e)).toList();
    creator = Creator.fromJson(json['creator']);
    dislike = json['dislike'];
    enabled = json['enabled'];
    hyperLinks = null;
    id = json['id'];
    idBase64 = json['id_base64'];
    isKaraoke = json['is_karaoke'];
    isKaraokeDownload = json['is_karaoke_download'];
    isKaraokeFinal = json['is_karaoke_final'];
    isLiked = json['is_liked'];
    isNewest = json['is_newest'];
    isQuestion = json['is_question'];
    isTeaser = json['is_teaser'];
    isTop = json['is_top'];
    isTranscode = json['is_transcode'];
    isUploaded = json['is_uploaded'];
    karaokeId = json['karaoke_id'];
    like = json['like'];
    liveVideo = json['live_video'];
    meta = null;
    musicians = List.castFrom<dynamic, dynamic>(json['musicians']);
    newestAt = json['newest_at'];
    payable = json['payable'];
    playList = null;
    poemFa = json['poem_fa'];
    position = json['position'];
    posterPath = json['poster_path'];
    published = json['published'];
    rate = json['rate'];
    readyToSendComment = json['ready_to_send_comment'];
    releaseDate = json['release_date'];
    toncodes = List.castFrom<dynamic, dynamic>(json['toncodes']);
    uploadPath = json['upload_path'];
    uploadedAt = json['uploaded_at'];
    url = json['url'];
    videoCaption = json['video_caption'];
    videoDemoPath = json['video_demo_path'];
    videoDescription = json['video_description'];
    videoExt = json['video_ext'];
    videoPrice = json['video_price'];
    videoPriceRial = json['video_price_rial'];
    videoPriceToman = json['video_price_toman'];
    videoSize = json['video_size'];
    videoStreamPath = json['video_stream_path'];
    view = json['view'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['artist_position'] = artistPosition;
    _data['artists'] = artists.map((e)=>e.toJson()).toList();
    _data['bands'] = bands;
    _data['categories'] = categories.map((e)=>e.toJson()).toList();
    _data['challenge_teaser'] = challengeTeaser;
    _data['comments'] = comments;
    _data['comments_count'] = commentsCount;
    _data['company'] = company.map((e)=>e.toJson()).toList();
    _data['creator'] = creator.toJson();
    _data['dislike'] = dislike;
    _data['enabled'] = enabled;
    _data['hyper_links'] = hyperLinks;
    _data['id'] = id;
    _data['id_base64'] = idBase64;
    _data['is_karaoke'] = isKaraoke;
    _data['is_karaoke_download'] = isKaraokeDownload;
    _data['is_karaoke_final'] = isKaraokeFinal;
    _data['is_liked'] = isLiked;
    _data['is_newest'] = isNewest;
    _data['is_question'] = isQuestion;
    _data['is_teaser'] = isTeaser;
    _data['is_top'] = isTop;
    _data['is_transcode'] = isTranscode;
    _data['is_uploaded'] = isUploaded;
    _data['karaoke_id'] = karaokeId;
    _data['like'] = like;
    _data['live_video'] = liveVideo;
    _data['meta'] = meta;
    _data['musicians'] = musicians;
    _data['newest_at'] = newestAt;
    _data['payable'] = payable;
    _data['play_list'] = playList;
    _data['poem_fa'] = poemFa;
    _data['position'] = position;
    _data['poster_path'] = posterPath;
    _data['published'] = published;
    _data['rate'] = rate;
    _data['ready_to_send_comment'] = readyToSendComment;
    _data['release_date'] = releaseDate;
    _data['toncodes'] = toncodes;
    _data['upload_path'] = uploadPath;
    _data['uploaded_at'] = uploadedAt;
    _data['url'] = url;
    _data['video_caption'] = videoCaption;
    _data['video_demo_path'] = videoDemoPath;
    _data['video_description'] = videoDescription;
    _data['video_ext'] = videoExt;
    _data['video_price'] = videoPrice;
    _data['video_price_rial'] = videoPriceRial;
    _data['video_price_toman'] = videoPriceToman;
    _data['video_size'] = videoSize;
    _data['video_stream_path'] = videoStreamPath;
    _data['view'] = view;
    return _data;
  }
}
Future<AutoGenerate> getData() async{
  final response=await http.get(Uri.parse('https://notenet.ir/mobile/index'));
  if(response.statusCode==200){
    return AutoGenerate.fromJson(jsonDecode(response.body));
  }
  else{
    throw Exception('Failed to load data');
  }

}
