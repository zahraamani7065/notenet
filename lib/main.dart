

import 'package:flutter/material.dart';
import 'package:notenet/Widgets/BestVideos.dart';
import 'package:notenet/Widgets/CarouselSlider.dart';
import 'package:notenet/Widgets/MusicStyle.dart';
import 'package:notenet/Widgets/Vip.dart';
import 'package:notenet/carousel/carousel_controller.dart';
import 'package:notenet/carousel/carousel_slider.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'Data/data.dart';
import 'Widgets/Banner.dart';
import 'Widgets/NewestVideo.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      builder: (context, child) => ResponsiveWrapper.builder(
          child,
          maxWidth: 1200,
          minWidth: 480,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(480, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
          background: Container(color: Color(0xFFF5F5F5))),
      initialRoute: "/",

      theme: ThemeData(
        textTheme: const TextTheme(
          headline6: TextStyle(
              fontSize: 26, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      home: const HomePage(),
    );
  }}
class HomePage extends StatefulWidget{
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title:  Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children:  [
            Text('نُت نِت',style: Theme.of(context).textTheme.headline6,),
            const SizedBox(width: 3,),
            const Icon(Icons.music_note),
          ],),
      ),
      body:
      FutureBuilder<AutoGenerate>(
        future: getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return SafeArea(
                child:SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Column(
                        children: [

                          const SizedBox(height: 10,),
                          CarouselSliders(banner: snapshot.data!.banner,),
                          const SizedBox(height: 12,),
                          VipMusic(VIPlst: snapshot.data!.vipVideos,),
                          const SizedBox(height: 12,),
                          newestVideo(newestVideoLst: snapshot.data!.newestvideo,),
                          const SizedBox(height: 12,),
                          BestVideos(bestVideos: snapshot.data!.mostpopularvideo),
                          const SizedBox(height: 22,),
                          MusicStyle(chalengs: snapshot.data!.challenge,)
                        ]
                    )
                )
            );
          }
          else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),

    );
  }
}



